console.log("Hello World");

let myPokemon = {
	name: 'Pikachu',
	Age: 3,
	Pokemon: ['Charmander', 'Balbazoar', 'arcob'],
	Friends: ['Ash', 'Brock', 'Iris'],
	talk: function(){
		console.log(` ${this.name} I choose you`);
	}
}

myPokemon.talk();
console.log(myPokemon);

console.log(myPokemon.name);
console.log(myPokemon.Pokemon[0]);

myPokemon.talk();
console.log(myPokemon);


function Pokemon(name, level) {
	//properites
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		target.health -= this.attack
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
		
		if(target.health <= 0) {
			target.faint()
		}
	}
	this.faint = function() {
		console.log( this.name + ' fainted.');
	}
}
let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);

let charmander = new Pokemon("Charmander", 8);
console.log(charmander);

let rattata = new Pokemon("Rattata", 50)
console.log(rattata)

charmander.tackle(pikachu);
pikachu.tackle(charmander);
rattata.tackle(pikachu);